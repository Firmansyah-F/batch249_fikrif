var rl = require('readline-sync')
var abjad = ("a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z")
var Input = rl.question("Masukkan kode : ")
var jumlah_rotasi = rl.questionInt("Masukkan jumlah rotasi : ")
var arraykalimat = abjad.split(',') 
var kode = Input.split('')
var output = []
function rotasiArray(arraykalimat, jumlah_rotasi) {
    if (jumlah_rotasi >= 0) {
      var potong = arraykalimat.splice(arraykalimat, jumlah_rotasi);
      var output = [...arraykalimat,...potong];
      return output;
    } 
    else {
      return "Format Salah";
    }
  }

var rotasi = rotasiArray(arraykalimat,jumlah_rotasi)
var rotasi = rotasi.toString()
console.log("rotasi abjad " + jumlah_rotasi + 'x = ' , rotasi)

for(let i=0; i<kode.length; i++){
    // var huruf = input[i].toLowerCase()
    if(abjad.includes(kode[i]))
    {   
        var z = abjad.indexOf(kode[i]) //b digunakan untuk mengetahui index dari alphabet yang di inputkan
        //console.log(b)
        output.push(rotasi[z])
    }
    else if( (abjad.toUpperCase()).includes(kode[i]) )
    {
        var x = (abjad.toUpperCase()).indexOf(kode[i]) // c digunakan untuk mengetahui index dari input yang uppercase
        output.push(rotasi[x].toUpperCase())
    }
    else
    {
        output.push(kode[i])
    }

} 

console.log(output.join(''))