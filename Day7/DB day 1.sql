/*create table tblPengarang (
    ID bigint Primary Key,
    Kd_Pengarang varchar(7) not null,
    Nama varchar (30) not null,
    Alamat varchar (80) not null,
    Kota varchar (15) not null,
    Kelamin varchar (1) not null
)*/
/*INSERT INTO tblPengarang(Kd_pengarang, Nama, Alamat, Kota, Kelamin)
values
	('P0001', 'Ashadi', 'Jl.Beo 25','Yogya','P'),
	('P0002', 'Rian', 'Jl.Solo 123','Yogya','P'),
	('P0003', 'Suwadi', 'Jl.Semangka 13','Bandung','P'),
	('P0004', 'Siti', 'Jl.Durian 15','Solo','W'),
	('P0005', 'Amir', 'Jl.Gajah 33','Kudus','P'),
	('P0006','Suparman','Jl. Harimau 25','Jakarta','P'),
	('P0007','Jaja','Jl. Singa 7','Bandung','P'),
	('P0008','Saman','Jl. Naga 12','Yogya','P'),
	('P0009','Anwar','Jl. Tidar 6A','Magelang','P'),
	('P0010','Fatmawati','Jl. Renjana 4','Bogor','W');
*/
/*Create Table tblGaji (
	ID bigint Primary Key,
    Kd_Pengarang varchar(7) not null,
    Nama varchar (30) not null,
	Gaji decimal (18,4) not null
)*/

/*INSERT INTO tblGaji(Kd_pengarang, Nama, Gaji)
values
	('P0002', 'Rian', '600000'),
	('P0003', 'Suwadi', '1000000'),
	('P0004', 'Siti', '500000'),
	('P0005', 'Amir','700000'),
	('P0008','Saman','750000'),
	('P0010','Fatmawati','600000');
*/
--Select COUNT (Kd_pengarang) as jml_pengarang			--No 1
--From tblPengarang

-- Select kelamin, COUNT (Kelamin) as jml_kelamin					--No 2
-- From tblPengarang
-- group by Kelamin

--Select kota, COUNT (kota) as jml_kota					--No 3
--From tblPengarang
--group by kota

--Select kota											--No 4
--From tblPengarang
--group by kota
--having count(kota) > 1

-- SELECT kd_pengarang FROM tblpengarang				--NO 5
-- order by kd_pengarang ASC
-- SELECT kd_pengarang FROM tblpengarang
-- order by kd_pengarang DESC

--SELECT MIN(gaji), MAX(gaji) FROM tblGaji							--NO 6


-- SELECT gaji FROM tblGaji								--NO 7
-- where gaji > 600000

--Select sum (gaji) as jumlah_gaji						--No 8
-- FROM tblGaji

-- select p.kota, sum (g.gaji)								--No 9
-- from  tblGaji as g
-- inner join tblPengarang as p
-- on p.kd_Pengarang = g.kd_Pengarang
-- group by kota

--select p.Kd_pengarang, p.nama						--NO 10
--from tblPengarang as p
--order by kd_pengarang
--LIMIT 6

-- SELECT Nama ,kota FROM tblPengarang						--NO 11
-- where kota= 'Yogya' or kota='Solo' or kota='Magelang' 

-- SELECT Nama ,kota FROM tblPengarang						--NO 12
-- where kota != 'Yogya' 

-- SELECT Nama FROM tblPengarang
-- Where Nama like 'A%' or Nama like '%i' or Nama like '__a%' or Nama not like '%n'   --NO 13

-- select p.Kd_pengarang, g.gaji 									--NO 14
-- from tblPengarang as p
-- inner join tblGaji as g
-- on p.kd_Pengarang = g.kd_Pengarang

-- select p.kota, g.gaji											--No 15
-- from tblPengarang as p
-- inner join tblGaji as g
-- on p.kd_Pengarang = g.kd_Pengarang
-- where g.gaji < 1000000

-- ALTER TABLE tblpengarang											--No 16
-- ALTER Kelamin type varchar (10)

-- ALTER TABLE tblpengarang											--No 17
-- ADD Gelar varchar (12)

-- UPDATE tblpengarang												--No 18
-- SET alamat = 'Jl. Cendrawasih 65', kota = 'Pekanbaru'
-- WHERE nama = 'Rian';

-- create view vw_pengarang as										--No19
-- select p.kd_pengarang, p.Nama, p.kota 
-- from tblPengarang as p

-- select * from tblpengarang
-- order by id 

-- select * from vw_pengarang

