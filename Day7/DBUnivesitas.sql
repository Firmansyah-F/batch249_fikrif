-- Create Table jurusankuliah (
-- 	ID bigint Primary Key,
-- 	kode_jurusan varchar(12),
-- 	deskripsi varchar (50)
-- )

-- INSERT INTO jurusankuliah (kode_jurusan,deskripsi)
-- Values
-- 	('KA001',	'Teknik Informatika'),
-- 	('KA002',	'Management Bisnis'),
-- 	('KA003',	'Ilmu Komunikasi'),
-- 	('KA004',	'Sastra Inggris'),
-- 	('KA005',	'Ilmu Pengetahuan Alam Dan Matematika'),
-- 	('KA006',	'Kedokteran');

-- Create Table dosen (
-- 	ID bigint Primary Key,
-- 	kode_dosen varchar(14),
-- 	nama_dosen varchar (15),
-- 	kode_matakuliah varchar (12),
-- 	kode_fakultas varchar (13)
-- )

-- INSERT INTO dosen (kode_dosen, nama_dosen, kode_matakuliah, kode_fakultas)
-- Values
-- 	('GK001',	'Ahmad Prasetyo',	'KPK001',	'TK002'),
-- 	('GK002',	'Hadi Fuladi',		'KPK002',	'TK001'),
-- 	('GK003',	'Johan Goerge',		'KPK003',	'TK002'),
-- 	('GK004',	'Bima Darmawan',	'KPK004',	'TK002'),
-- 	('GK005',	'Gatot Wahyudi'	,	'KPK005',	'TK001');

-- Create Table matakuliah (
-- 	ID bigint Primary Key,
-- 	kode_matakuliah varchar (12),
-- 	nama_matakuliah varchar(30),
-- 	keaktifan_status varchar (15)
-- )

-- INSERT INTO matakuliah (kode_matakuliah, nama_matakuliah, keaktifan_status)
-- Values
-- 	('KPK001',	'Algoritma Dasar',	'Aktif'),
-- 	('KPK002',	'Basis Data',	'Aktif'),
-- 	('KPK003',	'Kalkulus',	'Aktif'),
-- 	('KPK004',	'Pengantar Bisnis',	'Aktif'),
-- 	('KPK005',	'Matematika Ekonomi & Bisnis',	'Non Aktif');

-- Create Table mahasiswa (
-- 	ID bigint Primary Key,
-- 	kode_mahasiswa		varchar	(12),
-- 	nama_mahasiswa		varchar	(20),
-- 	alamat_mahasiswa	varchar	(30),
-- 	kode_jurusan		varchar	(12),
-- 	kode_matakuliah		varchar	(12)
-- )

-- INSERT INTO mahasiswa (kode_mahasiswa, nama_mahasiswa,
-- 					   alamat_mahasiswa, kode_jurusan, kode_matakuliah)
-- Values
-- 	('MK001',	'Fullan',			'Jl. Hati besar no 63 RT.13',	'KA001',	'KPK001'),
-- 	('MK002',	'Fullana Binharjo',	'Jl. Biak kebagusan No. 34', 	'KA002',	'KPK002'),
-- 	('MK003',	'Sardine Himura',	'Jl. Kebajian no 84',			'KA001',	'KPK003'),
-- 	('MK004',	'Isani isabul',		'Jl. Merak merpati No.78',		'KA001',	'KPK001'),
-- 	('MK005',	'Charlie birawa',	'Jl. Air terjun semidi No.56',	'KA003',	'KPK002');

-- Create Table nilaimahasiswa (
-- 	ID bigint Primary Key,
-- 	kode_nilai	varchar	(12),
-- 	kode_mahasiswa	varchar	(12),
-- 	kode_organisasi	varchar	(9),
-- 	nilai  int NULL
-- )

-- INSERT INTO nilaimahasiswa (kode_nilai, kode_mahasiswa, kode_organisasi, nilai)
-- Values
-- 	('SK001',	'MK004',	'KK001',	'90'),
-- 	('SK002',	'MK001',	'KK001',	'80'),
-- 	('SK003',	'MK002',	'KK003',	'85'),
-- 	('SK004',	'MK004',	'KK002',	'95'),
-- 	('SK005',	'MK005',	'KK005',	'70');

-- Create Table fakultas (
-- 	ID bigint Primary Key,
-- 	kode_fakultas varchar (13),
-- 	penjelasan varchar (30)
-- )

-- INSERT INTO fakultas (kode_fakultas,penjelasan)
-- Values
-- 	('TK001',	'Teknik Informatika'),
-- 	('TK002',	'Matematika'),
-- 	('TK003',	'Sistem Infromatika');

-- Create Table organisasi (
-- 	ID bigint Primary Key,
-- 	kode_organisasi	varchar	(12),
-- 	nama_organisasi	varchar	(50),
-- 	status_organisasi	varchar	(13)
-- )


-- INSERT INTO organisasi (kode_organisasi,nama_organisasi, status_organisasi)
-- Values
-- 	('KK001',	'Unit Kegiatan Mahasiswa (UKM)', 					'Aktif'),
-- 	('KK002',	'Badan Eksekutif Mahasiswa Fakultas (BEMF)',		'Aktif'),
-- 	('KK003',	'Dewan Perwakilan Mahasiswa Universitas (DPMU)',	'Aktif'),
-- 	('KK004',	'Badan Eksekutif Mahasiswa Universitas (BEMU)',		'Non Aktif'),
-- 	('KK005',	'Himpunan Mahasiswa Jurusan',						'Non Aktif'),
-- 	('KK006',	'Himpunan Kompetisi Jurusan', 						'Aktif');


-- --No2
-- ALTER TABLE dosen
-- alter column nama_dosen type varchar (200)

-- --No3
-- SELECT m.kode_mahasiswa, m.nama_mahasiswa, k.nama_matakuliah, j.deskripsi
-- from mahasiswa as m
-- inner join matakuliah as k
-- on m.kode_matakuliah = k.kode_matakuliah
-- inner join jurusankuliah as j
-- on m.kode_jurusan = j.kode_jurusan
-- limit 1

-- --No4
-- SELECT k.nama_matakuliah, k.keaktifan_status, m.kode_mahasiswa, 
-- m.nama_mahasiswa, m.alamat_mahasiswa , j.kode_jurusan, k.kode_matakuliah

-- from mahasiswa as m
-- full outer join matakuliah as k
-- on m.kode_matakuliah = k.kode_matakuliah
-- full outer join jurusankuliah as j
-- on m.kode_jurusan = j.kode_jurusan
-- where k.keaktifan_status = 'Non Aktif'

-- --No5
-- SELECT m.kode_mahasiswa, m.nama_mahasiswa, m.alamat_mahasiswa 

-- from mahasiswa as m
-- join nilaimahasiswa as n
-- on m.kode_mahasiswa = n.kode_mahasiswa
-- join organisasi as o
-- on n.kode_organisasi = o.kode_organisasi
-- group by m.kode_mahasiswa, m.nama_mahasiswa, m.alamat_mahasiswa, o.status_organisasi
-- having sum (n.nilai) > 79  and o.status_organisasi = 'Aktif'

-- --No6
-- SELECT kode_matakuliah, nama_matakuliah, keaktifan_status
-- FROM matakuliah
-- where nama_matakuliah like '%n%' OR nama_matakuliah like '%N%'

-- --No7
-- SELECT m.nama_mahasiswa, count (m.kode_mahasiswa) as jml_organisasi 
-- from mahasiswa as m
-- join nilaimahasiswa as n
-- on m.kode_mahasiswa = n.kode_mahasiswa
-- group by m.nama_mahasiswa
-- having count (m.nama_mahasiswa) > 1

-- No8
-- SELECT m.kode_mahasiswa, m.nama_mahasiswa, k.nama_matakuliah ,
-- j.deskripsi, d.nama_dosen, k.keaktifan_status, f.penjelasan

-- from mahasiswa as m
-- join matakuliah as k
-- on m.kode_matakuliah = k.kode_matakuliah
-- join jurusankuliah  as j
-- on m.kode_jurusan = j.kode_jurusan
-- join dosen  as d
-- on k.kode_matakuliah = d.kode_matakuliah
-- join fakultas as f
-- on d.kode_fakultas = f.kode_fakultas
-- where m.kode_mahasiswa = 'MK001'

-- --No9
-- create view vw_universitas as									
-- SELECT m.kode_mahasiswa, m.nama_mahasiswa, k.nama_matakuliah ,
-- d.nama_dosen, k.keaktifan_status, f.penjelasan

-- from mahasiswa as m
-- join matakuliah as k
-- on m.kode_matakuliah = k.kode_matakuliah
-- join dosen  as d
-- on k.kode_matakuliah = d.kode_matakuliah
-- join fakultas as f
-- on d.kode_fakultas = f.kode_fakultas


-- select * from vw_universitas

-- --No10
-- select * from vw_universitas
-- where kode_mahasiswa = 'MK001'