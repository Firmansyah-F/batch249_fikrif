CREATE DATABASE ENTERTAINMENT

create table artis
(
	id bigint generated always as identity primary key not null,
	kd_artis varchar(100) not null,
    nm_artis varchar(100) NOT NULL,
	jk varchar(100) NOT NULL,
	bayaran integer not null,
	award integer not null,
	negara varchar(100) not null
)

create table negara
(
	id bigint generated always as identity primary key not null,
	kd_negara varchar(100) not null,
    nm_negara varchar(100) NOT NULL
)

create table genre
(
	id bigint generated always as identity primary key not null,
	kd_genre varchar(50) not null,
    nm_genre varchar(100) NOT NULL
)

create table produser
(
	id bigint generated always as identity primary key not null,
	kd_produser varchar(50) not null,
    nm_produser varchar(50) NOT NULL,
	international varchar(50) not null
)

create table film
(
	id bigint generated always as identity primary key not null,
	kd_film varchar(10) not null,
    nm_film varchar(55) NOT NULL,
	genre varchar(55) not null,
	artis varchar(55) not null,
	produser varchar(55) not null,
	pendapatan integer not null,
	nominasi integer not null
)

insert into artis
(
	kd_artis,
	nm_artis,
	jk,
	bayaran,
	award,
	negara
) values
	('A001','ROBERT DOWNEY JR','PRIA','500000000','2','AS'),
	('A002','ANGELINA JOLIE','WANITA','700000000','1','AS'),
	('A003','JACKIE CHAN','PRIA','200000000','7','HK'),
	('A004','JOE TASLIM','PRIA','350000000','2','ID'),
	('A005','CHELSEA ISLAN','WANITA','300000000','2','ID')

insert into negara
(
	kd_negara,
	nm_negara
) values
	('AS','AMERIKA SERIKAT'),
	('HK','HONGKONG'),
	('ID','INDONESIA'),
	('IN','INDIA')

insert into genre
(
	kd_genre,
	nm_genre
) values
	('G001','ACTION'),
	('G002','HORROR'),
	('G003','COMEDY'),
	('G004','DRAMA'),
	('G005','THRILLER'),
	('G006','FICTIONS')

insert into produser
(
	kd_produser,
	nm_produser,
	international
) values
	('PD01','MARVEL','YA'),
	('PD02','HONGKONG CINEMA','YA'),
	('PD03','RAPI FILM','TIDAK'),
	('PD04','PARKIT','TIDAK'),
	('PD05','PARAMOUNT PICTURE','YA')

insert into film
(
	kd_film,
	nm_film,
	genre,
	artis,
	produser,
	pendapatan,
	nominasi
) values
	('F001','IRON MAN','G001','A001','PD01','2000000000','3'),
	('F002','IRON MAN 2','G001','A001','PD01','1800000000','2'),
	('F003','IRON MAN 3','G001','A001','PD01','1200000000','0'),
	('F004','AVENGER CIVIL WAR','G001','A001','PD01','2000000000','1'),
	('F005','SPIDERMAN HOME COMING','G001','A001','PD01','1300000000','0'),
	('F006','THE RAID','G001','A004','PD03','800000000','5'),
	('F007','FAST & FURIOUS','G001','A004','PD05','830000000','2'),
	('F008','HABIBIE DAN AINUN','G004','A005','PD03','670000000','4'),
	('F009','POLICE STORY','G001','A003','PD02','700000000','3'),
	('F010','POLICE STORY 2','G001','A003','PD02','710000000','1'),
	('F011','POLICE STORY 3','G001','A003','PD02','615000000','0'),
	('F012','RUSH HOUR','G003','A003','PD05','695000000','2'),
	('F013','KUNGFU PANDA','G003','A003','PD05','923000000','5')

--nomor 1
select nm_film,nominasi 
from film
order by nominasi DESC;

--nomor 2
select nm_film , nominasi
from film
where nominasi = (
 select max (nominasi) as banyak_nominasi
	from film
);

--nomor 3
select nm_film, nominasi
from film
where nominasi = 0

--nomor 4
select nm_film, pendapatan
from film
order by pendapatan ASC;


--nomor 5
select nm_film, pendapatan
from film
where pendapatan = (
	select max (pendapatan) as pendapat_tertinggi
	from film
)

--nomor 6
select nm_film 
from film
where nm_film like 'P%';

--nomor 7
select nm_film
from film
where nm_film like '%H';

--nomor 8
select nm_film
from film
where nm_film like '%D%';

--nomor 9
select nm_film, pendapatan 
from film
where nm_film like '%O%' and pendapatan = (
	select max (pendapatan) as max_pendapatan
	from film
);

--nomor 10
select nm_film , pendapatan
from film
where nm_film like '%O%' and pendapatan = (
	select min(pendapatan) as min_pendapatan
	from film
);

--nomor 11
select f.nm_film , a.nm_artis 
from film as f
join artis as a
on a.kd_artis = f.artis

--nomor 12
select f.nm_film , n.nm_negara
from film as f
join artis as a
on a.kd_artis = f.artis
join negara as n
on a.negara = n.kd_negara
where n.kd_negara = 'HK'

--nomor 13
select f.nm_film , n.nm_negara
from film as f
join artis as a
on a.kd_artis = f.artis
join negara as n
on a.negara = n.kd_negara
where n.nm_negara not like '%O%' ;

--nomor 14
select f.nm_film , a.nm_artis
from film as f
join artis as a
on a.kd_artis = f.artis
where f.nm_film = null ;

--nomor 15
select a.nm_artis, g.nm_genre
from artis as a
join film as f
on a.kd_artis = f.artis 
join genre as g
on g.kd_genre = f.genre
where g.nm_genre = 'DRAMA' ;

--nomor 16
select a.nm_artis, g.nm_genre
from artis as a
join film as f
on a.kd_artis = f.artis 
join genre as g
on g.kd_genre = f.genre
where g.nm_genre = 'HORROR' ;

--nomor 17
select n.kd_negara, n.nm_negara, count (f.nm_film) as jumlah_film
from film as f
join artis as a
on f.artis = a.kd_artis
join negara as n
on a.negara = n.kd_negara
group by n.kd_negara, n.nm_negara;

--nomor 18
select nm_produser,international
from produser 
where international = 'YA'

--nomor 19
select p.nm_produser , count(f.nm_film) as jumlah_film
from produser as p
join film as f
on p.kd_produser = f.produser
group by p.nm_produser;

--nomor 20
select p.nm_produser, sum (f.pendapatan) as jumlah_pendapatan
from film as f
join produser as p
on f.produser = p.kd_produser
group by p.nm_produser
having p.nm_produser = 'MARVEL'