-- create table Karyawan (
--     ID bigint Primary Key,
-- 	nomor_induk varchar(7) NOT NULL,
--   	nama varchar(30) NOT NULL,
--   	alamat text NOT NULL,	
--   	tanggal_lahir date NOT NULL,
--   	tanggal_masuk date NOT NULL
-- )

-- INSERT INTO Karyawan(nomor_induk, nama, alamat, tanggal_lahir, tanggal_masuk)
-- values
-- 	('IP06001',	'Agus',	'Jln. Gajah Mada 115A, Jakarta Pusat',	'8/1/70',	'7/7/06'),
-- 	('IP06002',	'Amin',	'Jln. Bungur sari v No 178, Bandung',	'05/3/77',	'7/6/06'),
-- 	('IP06003',	'Yusuf','Jln. Yosodpuro 15, Surabaya',	'8/9/73',	'7/8/06'),
-- 	('IP07004',	'Alyssa','Jln. Cendana No. 6, Bandung',	'2/14/83',	'1/5/07'),
-- 	('IP07005',	'Maulana',	'Jln. Ampera Raya No 1',	'10/10/85',	'2/5/07'),
-- 	('IP07006',	'Afika'	,	'Jln. Pejaten Barat No 6A',	'3/9/87',	'6/9/07'),
-- 	('IP07007',	'James'	,	'Jln. Padjadjaran No. 111, bandung',	'5/19/88',	'6/9/06'),
-- 	('IP09008',	'Octavanus','Jln. Gajah Mada 101. Semarang',	'10/7/88',	'8/8/08'),
-- 	('IP09009',	'Nugroho',	'Jln. Duren Tiga 196, Jakarta selatan',	'1/20/88',	'11/11/08'),
-- 	('IP09010',	'Raisa',	'Jln. Nangka Jakarta selatan',	'12/29/89',	'2/9/09');

-- Create Table tblCuti (
-- 	id bigint PRIMARY KEY,
--   	nomor_induk varchar(7) NOT NULL,
--   	tanggal_mulai date NOT NULL,
--   	lama_cuti SMALLINT NOT NULL,
--   	keterangan text NOT NULL
-- )

-- INSERT INTO tblCuti(nomor_induk, tanggal_mulai, lama_cuti, keterangan)
-- values
-- 	('IP06001',	'2/1/12',	'3', 'Acara keluar'),
-- 	('IP06001',	'2/13/12',	'4', 'Anak sakit'),
-- 	('IP07007',	'2/15/12',	'2', 'Nenek sakit'),
-- 	('IP06003',	'2/17/12',	'1', 'Mendaftar sekolah anak'),
-- 	('IP07006',	'2/20/12',	'5', 'Menikah'),
-- 	('IP07004',	'2/27/12',	'1', 'Imunisasi anak');

--No1--
-- SELECT nama, tanggal_masuk FROM Karyawan		
-- order by tanggal_masuk ASC
-- limit 3

-- --No2--
-- Select k.nomor_induk, k.nama, c.tanggal_mulai, c.lama_cuti, c.keterangan
-- From tblcuti as c
-- inner join karyawan as k
-- on k.nomor_induk = c.nomor_induk
-- where c.tanggal_mulai <'2/16/2012' and (tanggal_mulai + lama_cuti) > '2/16/2012'

-- Select k.nomor_induk, k.nama, c.tanggal_mulai, c.lama_cuti, c.keterangan
-- From tblcuti as c
-- inner join karyawan as k
-- on k.nomor_induk = c.nomor_induk
-- where '2/16/2012' between c.tanggal_mulai and (tanggal_mulai + lama_cuti)

--No3--
-- Select c.nomor_induk, k.nama , count (c.nomor_induk) as jml_cuti
-- From tblcuti as c
-- inner join karyawan as k
-- on k.nomor_induk = c.nomor_induk
-- group by k.nama,c.nomor_induk
-- having count (c.nomor_induk) >1



-- INSERT INTO tblcuti(nomor_induk, tanggal_mulai, lama_cuti, keterangan)
-- values
-- 	('IP06002',	'7/6/06',	'0', '-'),
-- 	('IP07005',	'2/5/07',	'0', '-'),
-- 	('IP09008',	'8/8/08',	'0', '-'),
-- 	('IP09009',	'11/11/08',	'0', '-'),
-- 	('IP09010',	'2/9/09',	'0', '-');


--No4--
-- Select k.nomor_induk, k.nama , (12 - sum(c.lama_cuti)) as sisa_cuti
-- From tblcuti as c
-- full outer join karyawan as k
-- on k.nomor_induk = c.nomor_induk
-- group by k.nama,k.nomor_induk
-- order by nomor_induk

--nomor 5
-- Select c.nomor_induk, k.nama ,sum(c.lama_cuti) as jml_cuti
-- From tblcuti as c
-- inner join karyawan as k
-- on k.nomor_induk = c.nomor_induk
-- group by k.nama,c.nomor_induk
-- having sum(c.lama_cuti)>1;

